using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Examples.WebHook.Models;

namespace Telegram.Bot.Examples.WebHook.Data
{
    public static class DbInitializer
    {
        public static void Initialize(TgDBContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }

            var users = new UserModel[]
            {
                new UserModel{TId=707404258,NickName="Miker",OnCreated = DateTime.Now, Stage = 0},

            };
            foreach (UserModel e in users)
            {
                context.Users.Add(e);
            }
            context.SaveChanges();




            var groups = new string[] {
"СР-18","СР-20","СР-19","М-21",
"ФБСС-18","Мск-19","ОіО-21","Економіка-20","ОіО-20","ФБСС-20",
"УПЕП-19","ФБСС-21мз","Економіка-21","М-18","КН-20","ОіО-18","ФБСС-21",
"Мск-21","М-19","ФБСС-19","ОіОск-19з","ЕП-18","М-20","ОіО-19","СР-21",
"С-21","П-18","С-19","С-20","КН-21","КН-19","КН-18","П-19","П-20","П-21",
"ММ-19","П-20ск","ММ-18","ЕП-19","ФБССск-19","ФБСС-20ск","ОіОск-21","КН-20ск",
"Менеджмент-20","МЗД-20ск","МНС-19","МНС-18","УПЕП-19з","МНСск-19","МВск-19",
"Менеджмент-21","Т-19","Т-21","МЗД-21мз","Т-20","МЗД-18","МЗД-19","МТ-18",
"МЕБ-19","Т-18","МЗДск-21","КНск-19","КНск-21","КНск-18","МПД-18","УПЕПск-21","Тск-19","МЕБ-19з",
};

            List<GroupModel> groupsModel = new List<GroupModel>();
            foreach (var item in groups)
            {
                groupsModel.Add(new GroupModel() { FilePath = @"Files/КНск-1скороч..pdf", Name = item, OnCreated = DateTime.Now, OnUpdated = DateTime.Now });
            }

            foreach (GroupModel e in groupsModel)
            {
                context.Groups.Add(e);
            }
            context.SaveChanges();






        }
    }
}
