using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Examples.WebHook.Models;

namespace Telegram.Bot.Examples.WebHook.Data
{

    public class TgDBContext : DbContext
    {
        public TgDBContext(DbContextOptions<TgDBContext> options) : base(options)
        {

        }

        public DbSet<UserModel> Users { get; set; }
        public DbSet<GroupModel> Groups { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserModel>().ToTable("Users");
            modelBuilder.Entity<GroupModel>().ToTable("Groups");
        }
    }
}
