using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Bot.Examples.WebHook.Models
{
    public class GroupModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string? FilePath { get; set; }

        public List<UserModel> Users { get; set; } = new List<UserModel>();

        public DateTime OnCreated { get; set; }
        public DateTime? OnUpdated { get; set; }
    }
}
