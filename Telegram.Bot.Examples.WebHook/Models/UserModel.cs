using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Bot.Examples.WebHook.Models
{
    public class UserModel
    {
        public int ID { get; set; }
        public long TId { get; set; }
        public int Stage { get; set; }
        public string NickName { get; set; }

        public int? GroupId { get; set; }

        public DateTime OnCreated { get; set; }
    }
}
