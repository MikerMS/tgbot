using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Telegram.Bot.Examples.WebHook.Data;
using Telegram.Bot.Examples.WebHook.Models;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Examples.WebHook.Services
{
public class HandleUpdateService
{
        private readonly ITelegramBotClient _botClient;
        private readonly ILogger<HandleUpdateService> _logger;
        private readonly TgDBContext _context;

        public HandleUpdateService(ITelegramBotClient botClient, ILogger<HandleUpdateService> logger, TgDBContext context)
        {
            _botClient = botClient;
            _logger = logger;
            _context = context;
        }

        public async Task EchoAsync(Update update)
        {

           
            var handler = update.Type switch
            {
                // UpdateType.Unknown:
                // UpdateType.ChannelPost:
                // UpdateType.EditedChannelPost:
                // UpdateType.ShippingQuery:
                // UpdateType.PreCheckoutQuery:
                // UpdateType.Poll:
                UpdateType.Message            => BotOnMessageReceived(update.Message),
                UpdateType.EditedMessage      => BotOnMessageReceived(update.EditedMessage),
                UpdateType.CallbackQuery      => BotOnCallbackQueryReceived(update.CallbackQuery),
                UpdateType.InlineQuery        => BotOnInlineQueryReceived(update.InlineQuery),
                UpdateType.ChosenInlineResult => BotOnChosenInlineResultReceived(update.ChosenInlineResult),
                _                             => UnknownUpdateHandlerAsync(update)
            };

            try
            {
                await handler;
            }
            catch (Exception exception)
            {
                await HandleErrorAsync(exception);
            }
        }

        private async Task BotOnMessageReceived(Message message)
        {
            _logger.LogInformation($"Receive message type: {message.Type}");
            if (message.Type != MessageType.Text)
                return;

            UserModel user = _context.Users.FirstOrDefault(x => x.TId == message.From.Id);
            if (user is null)
            {
                user = _context.Users.Add(new UserModel { NickName = message.From.FirstName, OnCreated = DateTime.Now, Stage = 0 ,TId = message.From.Id}).Entity;
                _context.SaveChanges();
            }



            var action = user.Stage switch
            {
                0 => SendGroupSelection(_botClient, message,_context),
                1 => Usage(_botClient, message,user),
                //"/keyboard" => SendReplyKeyboard(_botClient, message),
                //"/remove" => RemoveKeyboard(_botClient, message),
                //"/photo" => SendFile(_botClient, message),
                //"/request" => RequestContactAndLocation(_botClient, message),
                _ => Usage(_botClient, message,user)
            };
            var sentMessage = await action;

            _logger.LogInformation($"The message was sent with id: {sentMessage.MessageId}");





            static async Task<Message> SendGroupSelection(ITelegramBotClient bot, Message message, TgDBContext context)
            {
                await bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);


                

                
                var groups = context.Groups.ToList();
                int count = groups.Count;
                int inRow = 1;
                List<List<InlineKeyboardButton>> rows = new List<List<InlineKeyboardButton>>();

                if (count % 5 == 0)         inRow = 5;
                else if (count % 4 == 0)    inRow = 4;
                else if (count % 3 == 0)    inRow = 3;
                else if (count % 2 == 0)    inRow = 2;

                for (int i = 0; i < count; i += inRow)
                {
                    var Row = new List<InlineKeyboardButton>();

                    for (int g = 0; g < inRow; g++)
                    {
                        var group = groups[i + g];
                        Row.Add(InlineKeyboardButton.WithCallbackData(group.Name, group.ID.ToString()));
                    }
                    rows.Add(Row);
                }

                var inlineKeyboard = new InlineKeyboardMarkup(rows);

                return await bot.SendTextMessageAsync(chatId: message.Chat.Id,
                                                      text: "Пожалуйтса укажите вашу группу",
                                                      replyMarkup: inlineKeyboard);
            }



            static async Task<Message> SendReplyKeyboard(ITelegramBotClient bot, Message message)
            {
                var replyKeyboardMarkup = new ReplyKeyboardMarkup(
                    new KeyboardButton[][]
                    {
                        new KeyboardButton[] { "1.1", "1.2" },
                        new KeyboardButton[] { "2.1", "2.2" },
                    })
                    {
                        ResizeKeyboard = true
                    };

                return await bot.SendTextMessageAsync(chatId: message.Chat.Id,
                                                      text: "Choose",
                                                      replyMarkup: replyKeyboardMarkup);
            }

            static async Task<Message> RemoveKeyboard(ITelegramBotClient bot, Message message)
            {
                return await bot.SendTextMessageAsync(chatId: message.Chat.Id,
                                                      text: "Removing keyboard",
                                                      replyMarkup: new ReplyKeyboardRemove());
            }

            static async Task<Message> SendFile(ITelegramBotClient bot, Message message)
            {
                await bot.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);

                const string filePath = @"Files/tux.png";
                using FileStream fileStream = new(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                var fileName = filePath.Split(Path.DirectorySeparatorChar).Last();

                return await bot.SendPhotoAsync(chatId: message.Chat.Id,
                                                photo: new InputOnlineFile(fileStream, fileName),
                                                caption: "Nice Picture");
            }

            static async Task<Message> RequestContactAndLocation(ITelegramBotClient bot, Message message)
            {
                var RequestReplyKeyboard = new ReplyKeyboardMarkup(new[]
                {
                    KeyboardButton.WithRequestLocation("Location"),
                    KeyboardButton.WithRequestContact("Contact"),
                });

                return await bot.SendTextMessageAsync(chatId: message.Chat.Id,
                                                      text: "Who or Where are you?",
                                                      replyMarkup: RequestReplyKeyboard);
            }

            static async Task<Message> Usage(ITelegramBotClient bot, Message message,UserModel user)
            {

                var action = message.Text switch
                {
                    "Получить расписание" => SendSchedule(bot, message, user),
                    _ => bot.SendTextMessageAsync(chatId: message.Chat.Id,text: "Пожалуйста выберите пункт меню")
                };

                return await action;
            }

            static async Task<Message> SendSchedule(ITelegramBotClient bot, Message message,UserModel user)
            {
                //await bot.SendChatActionAsync(message.Chat.Id, ChatAction.UploadDocument);

                //_context.Groups.FirstOrDefault(x=>x.ID == user.GroupId)

                //if (user is null)
                //{
                //    return await bot.SendTextMessageAsync(chatId: message.Chat.Id, text: "Нет группы!");
                //}

                //string filePath = user.Group?.FilePath;
                //using FileStream fileStream = new(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                //var fileName = filePath.Split(Path.DirectorySeparatorChar).Last();

                //return await bot.SendDocumentAsync(chatId: message.Chat.Id,
                //                                document: new InputOnlineFile(fileStream, fileName),
                //                                caption: $"Расписание для {user.Group.Name}");
                return null;
            }
        }

        // Process Inline Keyboard callback data
        private async Task BotOnCallbackQueryReceived(CallbackQuery callbackQuery)
        {
            UserModel user = _context.Users.FirstOrDefault(x => x.TId == callbackQuery.From.Id);
            if (user is not null)
            {
                Console.WriteLine(user.Stage);
                if (user.Stage == 0)
                {
                    
                    //user.Group = _context.Groups.FirstOrDefault(x => x.ID == int.Parse(callbackQuery.Data));
                    user.Stage = 1;
                    _context.SaveChanges();

                    await _botClient.DeleteMessageAsync(callbackQuery.Message.Chat.Id, callbackQuery.Message.MessageId);


                    await _botClient.AnswerCallbackQueryAsync(
                        callbackQueryId: callbackQuery.Id,
                        text: $"Received {_context.Groups.FirstOrDefault(x => x.ID == int.Parse(callbackQuery.Data)).Name}");

                    await _botClient.SendTextMessageAsync(
                        chatId: callbackQuery.Message.Chat.Id,
                        text: $"Received {_context.Groups.FirstOrDefault(x => x.ID == int.Parse(callbackQuery.Data)).Name}");

                    var replyKeyboardMarkup = new ReplyKeyboardMarkup(
                    new KeyboardButton[][]
                    {
                        new KeyboardButton[] { "Получить расписание", "Запрос на обновление расписания" },
                        new KeyboardButton[] { "2.1", "2.2" },
                    })
                    {
                        ResizeKeyboard = true
                    };

                    await _botClient.SendTextMessageAsync(chatId: callbackQuery.Message.Chat.Id,
                                                          text: "Вам доступны пункты меню",
                                                          replyMarkup: replyKeyboardMarkup);
                }
            }
            
            

            
        }

        #region Inline Mode

        private async Task BotOnInlineQueryReceived(InlineQuery inlineQuery)
        {
           _logger.LogInformation($"Received inline query from: {inlineQuery.From.Id}");

            InlineQueryResultBase[] results = {
                // displayed result
                new InlineQueryResultArticle(
                    id: "3",
                    title: "TgBots",
                    inputMessageContent: new InputTextMessageContent(
                        "hello"
                    )
                )
            };

            await _botClient.AnswerInlineQueryAsync(inlineQueryId: inlineQuery.Id,
                                                    results: results,
                                                    isPersonal: true,
                                                    cacheTime: 0);
        }

        private Task BotOnChosenInlineResultReceived(ChosenInlineResult chosenInlineResult)
        {
            _logger.LogInformation($"Received inline result: {chosenInlineResult.ResultId}");
            return Task.CompletedTask;
        }

        #endregion

        private Task UnknownUpdateHandlerAsync(Update update)
        {
            _logger.LogInformation($"Unknown update type: {update.Type}");
            return Task.CompletedTask;
        }

        public Task HandleErrorAsync(Exception exception)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _                                       => exception.ToString()
            };

            _logger.LogInformation(ErrorMessage);
            return Task.CompletedTask;
        }

    }
}
